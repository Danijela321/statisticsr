import webservice from "./srservice.js";

export default {
  //Studio Ett 1637
  //Tankesmedjan 3718

  /**
   * Funktionen returnerar en array med arrayer av typen [ord, antal förekomster]
   * sorterad på högsta antal förekomster.
   * 
   * OBS! Returnerar alltid en tom array om det uppstår något fel
   * @param {String} year
   */
  async getTopTenStudioEttWords(year) {
    let regex = /^$|•|vi|de|1700|för|vad|vi|som|när|man|har|om|att|mot|de|det|dem|en|i|och|på|-|av|aktuellt|med|efter|från|vår|är|vår|ny|år|ska|stora|mellan|hör|kan|nu|sedan|ut|hur|tar|ha|få|över|ett|bland|rapport|plats|lämnar|reportage|samtal|möt|:|eller|annat|än|gör|samt|så|–|andra|många|vara?|ta|upp|fd|dag/i;
    let wordArray = [];
    let descriptions = [];
    try {
      descriptions = await webservice.getProgramDescriptions(
        1637,
        `${year}-01-01`,
        `${year}-12-31`
      );
    } catch (e) {
      descriptions = [];
    }

    // Join all descriptions into one continuous string
    let allDescriptionsString = descriptions.join(" ");

    // remove punctuation and split into an array of words
    let words = allDescriptionsString.replace(/\.|,/g, "").split(" ");

    // filter out uninteresting words
    words = words.filter((element) => !element.match(regex));

    let wordCounts = [];

    for (let word of words) {
      word = word.toUpperCase();
      if (wordCounts[word]) {
        wordCounts[word]++;
      } else {
        wordCounts[word] = 1;
      }
    }

    wordArray = Object.entries(wordCounts);
    wordArray.sort((a, b) => b[1] - a[1]);
    return wordArray.slice(0, 10);
  },

  async hasBeenPlayed(songtitle) {
    let songsplayed = await webservice.getPlaylistForAllChannels();
    let matchingSongs = songsplayed.filter(
      (song) => song.title.toLowerCase() === songtitle.toLowerCase()
    );

    return matchingSongs.length > 0;
  },

  //returnerar bara kanalers id
  /*   async channels() {
      let resultID=[]
      resultID = await webservice.getAllChannelIds();    
      return resultID;
  },*/

  /**
   * Suggested usage:
   * 1. get a random song from the getRandomSong()-function
   * 2. present song.title to the end user and prompt for a guess on who the artist is
   * 3. give the song object and the user's guess as arguments to this function
   * 4. this function returns true if the user made a correct guess, otherwise false
   *
   * This function is case insensitive
   *
   * @param {object} song
   * @param {string} guessedArtist
   */
  guessArtist(song, guessedArtist) {
    return song.artist.toLowerCase() === guessedArtist.toLowerCase();
  },

  getRandomSong() {
    return webservice.getRandomSong();
  },

  getChannel(channelId) {
    return webservice.getChannelById(channelId);
  },

  getAudioByUrl(url) {
    return webservice.getAudioById(url);
  },

  //returnera id och kanalers namn
  getAllChannelSR() {
    return webservice.getAllChannel();
  },

  nowPlaying(channelId) {
    return webservice.nowPlaying(channelId);
  },
};
