/**
 * Here we do all remote API calls.
 *
 * Good to know for debugging
 * --------------------------
 * Channel IDs:
 * P3 = 164
 * ...
 */

/**
 * Helper method that returns a json response from an endpoint
 * @param {String} jsonendpoint
 */
async function fetchJson(jsonendpoint) {
  let response = await fetch(jsonendpoint);
  return await response.json();
}

export default {
  /**
   * Returns a playlist object (which contains song objects) for a given channel
   * @param {Number} channelId
   */
  async getPlaylist(channelId) {
    if (isNaN(channelId)) {
      return {};
    }

    let endpoint =
      "http://api.sr.se/api/v2/playlists/getplaylistbychannelid?id=" +
      channelId +
      "&format=json&size=500";
    let response = await fetchJson(endpoint);
    let playlist = response.song; // The playlist object is called "song" on the server, which can be confusing

    return playlist;
  },

  /**
   * Get a random song from the combined playlist of all channels
   */
  async getRandomSong() {
    let randomSonger = [];
    randomSonger = await this.getPlaylistForAllChannels();
    let x = Math.floor(Math.random() * (randomSonger.length - 1));
    let randomSong = randomSonger[x];
    return randomSong;
  },

  //Studio ett = 1637 datumjex: 2019-01-01
  /**
   * Get a list of descriptions (strings) for all episodes of a given program between two dates
   * Example:
   * Get the descriptions of all episodes of Studio Ett (id: 1637) broadcast in 2019
   * let descriptions = getProgramDescriptions(1637, "2019-01-01", "2019-12-31");
   * @param {Number} programId
   * @param {String} fromdate (yyyy-mm-dd)
   * @param {String} todate (yyyy-mm-dd)
   */
  async getProgramDescriptions(programId, fromdate, todate) {
    let endpoint = `http://api.sr.se/api/v2/episodes/index?programid=${programId}&fromdate=${fromdate}&todate=${todate}&format=json&size=700`;
    let response = await fetchJson(endpoint);

    let episodes = response.episodes;
    let descriptions = [];
    for (let episode of episodes) {
      descriptions.push(episode.description);
    }
    return descriptions;
  },
  //kolla att programmen har rätt id
  //Kolla att datumen ligger inom intervallet

  /**
   * Returns a "superplaylist" for all channels at Sveriges Radio.
   *
   * The superplaylist is the combination of all channels' playlists.
   * A playlist contains all songs that have been/will be broadcast from midnight today to midnight tomorrow
   * A song will occur as many times as it has been broadcast.
   */
  async getPlaylistForAllChannels() {
    let channels = await this.getAllChannelIds();

    let superplaylist = [];

    for (let channel of channels) {
      let playlist = await this.getPlaylist(channel);
      for (let song of playlist) {
        superplaylist.push(song);
      }
    }

    return superplaylist;
  },

  /**
   * Returns a list of all available channel ids
   */
  async getAllChannelIds() {
    let endpoint = "http://api.sr.se/api/v2/channels?format=json&size=500";

    let response = await fetchJson(endpoint);
    let result = [];
    let channels = response.channels;
    for (let channel of channels) {
      result.push(channel.id);
    }
    return result;
  },

  /**
   * A schedule is similar like a playlist, not for individual songs, but for episodes of programs
   * @param {Number} channelId
   */
  async getSchedule(channelId) {
    let endpoint = `http://api.sr.se/v2/scheduledepisodes?channelid=${channelId}&format=json&size=100`;
    let response = await fetchJson(endpoint);
    return response.schedule;

    //return fetchJson(endpoint);
  },

  /**
   * Returns information about a channel, given its id
   * @param {Number} channelId 
   */
  async getChannelById(channelId) {
    let endpoint = `http://api.sr.se/api/v2/channels/${channelId}?format=json`;
    let response = await fetchJson(endpoint);
    let channel = response.channel;
    return channel;
  },

  //hämtar alla kanaler som finns på SR och returnera id och name
  async getAllChannel() {
    let endpoint = "http://api.sr.se/api/v2/channels?format=json&size=500";
    let svar = await fetchJson(endpoint);
    let result = [];
    let allachannels = svar.channels;
    let allaNamn = "";
    let n = 0;

    for (let channel of allachannels) {
      n++;
      allaNamn =
        n +
        "." +
        '"' +
        channel.name +
        '"' +
        ", Radiostationsfrekvens:" +
        channel.id;
      result.push(allaNamn);
    }
    return result;
  },

  /**
   * Returns information about what is playing right now on a given channel
   * @param {Number} channelId
   */
  async nowPlaying(channelId) {
    let endpoint =
      "http://api.sr.se/api/v2/scheduledepisodes/rightnow?format=json&channelid=" +
      channelId;
    let svar = await fetchJson(endpoint);
    return svar;
  }
};
