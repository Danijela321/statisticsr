#!/bin/bash
# W3C validator API: https://validator.w3.org/docs/api.html

api="https://validator.w3.org/nu/"

commitFiles=$(git diff-tree --no-commit-id --name-only -r $CI_COMMIT_SHA)
files=$(git ls-tree --name-only --full-name -r $CI_COMMIT_SHA)
errors=""
badFiles=""
notValidated=""
servererrors=false
returnCode=0

falsePositiveCount=0
for file in $files; do

    if [ ${file##*.} = "html" ]; then
        # header="\"Content-Type: text/html; charset=utf-8\""
        echo "$file is html"
    elif [ ${file##*.} = "css" ]; then
        # header="\"Content-Type: text/css; charset=utf-8\""
        echo "$file is css"
    else
        echo "skipping ${file}"
        continue
    fi

    # Wait for a second as requested by the API documentation
    sleep 1

    echo "Sending ${file} to the validator"

    # respCmd="curl --silent -H ${header} --data-binary @$file ${api}?out=$out" ##Encoding problem
    # This works without the header
    response=$(curl -s -D - -F uploaded_file=@${file} ${api})
    # Get the status code
    status=$(echo "${response}" | grep "HTTP/2" | grep -o -e "\s[0-9]*\s" | grep -o -e "[0-9][0-9]*")

    if [[ $status -ne "200" ]]; then
        echo "ERROR: response status code is ${status}"
        servererrors=true
        notValidated="${notValidated}${file}\n"
        continue
    fi

    ##errors=${errors}$(echo ${response} | grep "\"type\":\"error\"") ##Om JSON hade fungerat...

    ######################
    # Parse the response #
    ######################
    echo "$response" > .resp
    sed -e '1,/^\r\{0,1\}$/d' -e 's/ /_/g' -e 's/<\/li>/<\/li>\n/g' -e 's/<li/\n<li/g' -e 's/<ol>/\n/g' .resp >.resp.sed

    response=$(cat .resp.sed)
    fileErrors=$(echo "${response}" | grep "class=\"error\"")
    fileWarnings=$(echo "${response}" | grep "class=\"info_warning\"")

    errorCount=0
    vueErrorCount=0
    warnings=0

    # If there are errors in this file
    if [[ $fileErrors != "" ]]; then
        # Loop through the errors and count them, ignore some false positives
        for error in $fileErrors; do
            errLine=$(echo "$error" | grep "not_allowed_as_child_of_element")
            if [ errLine == "" ]; then
                badFiles="${badFiles}${file}\n"
                errorCount=$(($errorCount + 1))
            else
                vueErrorCount=$(($vueErrorCount + 1))
            fi
        done
        if [ $vueErrorCount -gt 0 ]; then
            echo "Vue-related errors found in $file, ignoring them"
        fi
    fi

    # If there are warnings
    if [[ $fileWarnings != "" ]]; then
        # Count the warnings in this file
        for warning in $fileWarnings; do
            warnings=$(($warnings + 1))
        done

        if [ $warnings -gt 0 ]; then
            echo "WARNING: found $warnings warnings in $file"
            warnFiles="${warnFiles}${file}\n"
        fi
    fi

    # Present validation results for this file
    if [ $errorCount -eq 0 ]; then
        if [ $warnings -eq 0 ]; then
            echo "$file passed validation"
            # echo "caching $file"
            # echo -e "$file\n" >> success
        else
            echo "$file has no errors, but has $warnings warnings"
        fi
    else
        echo "$file has $errorCount errors and $warnings warnings"
    fi

    if [ $vueErrorCount -gt 0 ]; then
        echo "Found $vueErrorCount false positives in $file"
    fi

    falsePositiveCount=$(($falsePositiveCount+$vueErrorCount))
done


# Present validation results for all files
if [[ $badFiles != "" ]]; then
    echo "ERRORS DETECTED!"
    echo "The validator found errors in:"
    echo -e "$badFiles"
    echo "Please validate them manually at $api"
    returnCode=1
else
    echo "OK: found no errors in HTML/CSS"
    echo "OK: found $falsePositiveCount false positives"
    if [ "$warnFiles" != "" ]; then
        echo "WARNING: There are warnings in: "
        echo -e "$warnFiles"
    fi
    if [ "$servererrors" = true ]; then
        echo "WARNING: unable to validate the following files:"
        echo -e "$notValidated"
        echo "Please validate them manually at $api"
    fi
    returnCode=0
fi

exit $returnCode