# StatisticSR

En webapplikation som för statistik över Sveriges radios olika program.

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your unit tests
```
npm run test:unit
```

### Run your end-to-end tests
```
npm run test:e2e
```

### Lints and fixes files
```
npm run lint
```

### Watches for changes in the SCSS file and converts it to CSS
```
npm run css:watch
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
