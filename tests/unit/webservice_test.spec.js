//http://api.sr.se/api/v2/playlists/getplaylistbychannelid?id=164&format=json&size=500
import { expect } from "chai";
import webservice from "@/services/srservice";
import fetchMock from "fetch-mock/es5/server";

// describe("getPlaylist()", () => {
//   it("returns empty object when called with parameter that is not a number", () => {});
//   it("returns an object with an array named song when called with id 164", () => {});
// });

describe("webservice.getChannelById()", () => {
  before(() => {
    fetchMock.get("http://api.sr.se/api/v2/channels/164?format=json", {
      channel: 164
    });
  });

  it("returns a correct channel", async () => {
    let channel = await webservice.getChannelById(164);
    expect(channel).to.equal(164);
    expect(fetchMock.called("http://api.sr.se/api/v2/channels/164?format=json"))
      .to.be.true;
  });

  after(() => fetchMock.restore());
});

describe("webservice.getAllChannelIds()", () => {
  before(() => {
    fetchMock.get("http://api.sr.se/api/v2/channels?format=json&size=500", {
      copyright: "copyright",
      channels: [
        { name: "p1", id: 132 },
        { name: "p2", id: 163 },
        { name: "p3", id: 164 }
      ],
      pagination: "1"
    });
  });

  it("does return an array with all channelIds", async () => {
    let channelIds = await webservice.getAllChannelIds();
    expect(channelIds).to.have.lengthOf(3);
    expect(channelIds).to.eql([132, 163, 164]);
    expect(
      fetchMock.called("http://api.sr.se/api/v2/channels?format=json&size=500")
    ).to.be.true;
  });

  after(() => fetchMock.restore());
});

//testa att de inte returnerar null
//testa att resultatet innehåller något särskilt
