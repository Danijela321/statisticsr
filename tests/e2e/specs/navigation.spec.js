// Enable code-completion for Cypress
/// <reference types="Cypress" />

/*
 * Here we test everything navigation-related
 */

describe("Navigate from root (home) view to other views", () => {
  let siteRoot = "http://localhost:8080/#/";

  /**
   * Before each test, go back to the root URL
   */
  beforeEach(() => {
    cy.visit("/");
  });

  it("Visits the app root url", () => {
    visit("");
    cy.contains("h2", "Välkommen");
  });

  it("Visits the about url", () => {
    visit("about");
    cy.contains("h1", "Om oss");
  });

  it("Visits the channels url", () => {
    visit("channels");
    cy.get("article div button");
  });

  it("Visits the guesser url", () => {
    visit("guesser");
    cy.contains("Who made");
  });

  it("Visits the played url", () => {
    visit("played");
    cy.get("input");
    cy.get("div button");
  });

  it("Visits the schedule url", () => {
    visit("schedule");
    cy.contains("Tablåer");
  });

  it("Visits the toplist url", () => {
    visit("toplist");
    cy.contains("Populära ord");
  });

  function visit(endpoint) {
    cy.visit("#/" + endpoint);
    cy.get("#channelList"); // All views should have the channelList
    cy.url().should("match", new RegExp(siteRoot + endpoint));
  }
});
